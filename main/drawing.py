#import pygame
from sprite import *
import os


SPRITESIZE = [16, 16]
PLAYERSIZE = [16, 24]

PLAYER_SHEET = SpriteSheet(PLAYER_PATH, PLAYERSIZE)
TILES_SHEET = SpriteSheet(TILE_PATH, SPRITESIZE)
POWERUP_SHEET = SpriteSheet(POWERUP_PATH, SPRITESIZE)
SHEETS = {PLAYER_SHEET, TILES_SHEET, POWERUP_SHEET}
PLAYER_SHEET.scale((32, 48))
TILES_SHEET.scale((32, 32))
POWERUP_SHEET.scale((32, 32))

COLOUR_DICT = {"floor": (16, 120, 48), "barrier": (64, 64, 64), "wall": (128, 128, 128), "flame": (207, 53, 46),
                   "wall_destroyed": (100, 100, 100), "spawn": (100, 110, 38), "bomb": (16, 120, 48)}


background_blits = 0
flame_blits = 0
player_blits = 0
bomb_blits= 0
other_tile_blits = 0

#TODO implement different levels of staticness for different types of tiles
#eg barrier tiles never change, so they can be blitted
#to a surface only once; walls only occasionally change and are large in number, so they can be blitted to a surface
#once at start, and once every time a wall is created or destroyed; and bombs/flames/players are small in number
#and only on screen temporarily so they can be kept the same, blitted every frame

def create_animations(players, tiles, powerups):
    # TODO remake to allow any number of colours to be added - based on spritesheet size

    colours = ["white", "blue", "black", "red"]

    # CREATE PLAYER ANIMATIONS ############################################################################
    player_animations = dict()

    for i, colour in enumerate(colours):  # for each player colour
        player_animations[colour] = dict()

        walk = list()
        walking_delays = [300, 200, 300, 200]

        stand = list()

        for j in range(4):  # for each direction
            j *= 3
            sequence = [(j + 1, i), (j, i), (j + 2, i), (j, i)]
            animation = Animation(players, sequence, walking_delays)
            walk.append(animation)

            animation = Animation(players, [(j, i)], 0)
            stand.append(animation)

        punch = list()
        for j in range(4):
            j += 18
            animation = Animation(players, [(j, i)], 300, False)
            punch.append(animation)

        death = Animation(players, [(j, i) for j in range(12, 18)], 1000, False)

        player_animations[colour]["walk"] = walk
        player_animations[colour]["stand"] = stand
        player_animations[colour]["punch"] = punch
        player_animations[colour]["death"] = death

    # CREATE TILE ANIMATIONS ##############################################################################
    tile_animations = dict()

    barrier             = Animation(tiles, [[1, 0]], 0)
    wall                = Animation(tiles, [[2, 0]], 0)
    wall_destroyed      = Animation(tiles, [[i, 3] for i in range(6)], 500)
    powerup_destroyed   = Animation(tiles, [[i, 4] for i in range(7)], 500)
    bomb                = Animation(tiles, [[0, 1], [1, 1], [2, 1], [1, 1]], 1200)
    flame               = Animation(tiles, [[i, 10] for i in range(5)], 720)

    tile_animations["barrier"] = barrier
    tile_animations["wall"] = wall
    tile_animations["wall_destroyed"] = wall_destroyed
    tile_animations["powerup_destroyed"] = powerup_destroyed
    tile_animations["bomb_entity"] = bomb
    tile_animations["flame"] = flame

    # CREATE POWERUP ANIMATIONS ###########################################################################
    powerup_animations = dict()

    return player_animations, tile_animations, powerup_animations


PLAYER_ANIMATIONS, TILE_ANIMATIONS, POWERUP_ANIMATIONS = create_animations(PLAYER_SHEET, TILES_SHEET, POWERUP_SHEET)

def convert_sheets():
    PLAYER_SHEET.convert()
    TILES_SHEET.convert()
    POWERUP_SHEET.convert()


def create_background(board):
    board_size = board.get_size()
    tile_size = board.get_tile_size()

    bg = pygame.Surface((board_size[0] * tile_size[0], board_size[1] * tile_size[1]))
    bg.fill(COLOUR_DICT["floor"])

    for x in range(board_size[0]):
        for y in range(board_size[1]):
            tile_name = board.tile_properties((x, y))["name"]
            if tile_name == "barrier":
                tile = TILE_ANIMATIONS[tile_name].get_frame(0)
                bg.blit(tile, (x * tile_size[0], y * tile_size[1]))

    return bg


def draw_board(board, background=None):
    global background_blits, flame_blits, other_tile_blits

    board_size = board.get_size()
    tile_size = board.get_tile_size()
    surface = pygame.Surface((board_size[0] * tile_size[0], board_size[1] * tile_size[1]))

    if background is not None:
        surface.blit(background, (0,0))
        background_blits += 1
    else:
        surface.fill(COLOUR_DICT["floor"])
        print("filled floor")

    # tile_width, tile_height = board.get_tile_size()

    #for every i, j in range of 2d list (15, 15):
    #   get tile object at position i, j
    #   get the animation from a dict at key tile.name (10)
    #   get the current frame of animation (5):
    #       calculate the current index of animation from tile age
    #       get the sprite sheet index of current animation index eg: [[0,0], [1,0], [2,0]][current_index]
    #       get image from 2d array made by spritesheet

    #speedup: blit all barrier tiles to background surface once, and blit this surface instead of each tile separately

    for x in range(board_size[0]):
        for y in range(board_size[1]):
            tile_name = board.tile_properties((x, y))["name"]
            if background is not None and tile_name == "barrier":
                continue
            tile_birth = board.tile_properties((x, y))["birth"]

            if tile_name in TILE_ANIMATIONS:
                tile = TILE_ANIMATIONS[tile_name].get_current_frame(tile_birth)
                surface.blit(tile, (x * tile_size[0], y * tile_size[1]))
                if tile_name == "flame":
                    flame_blits += 1
                else:
                    other_tile_blits += 1
            elif tile_name != "floor":
                colour = COLOUR_DICT[tile_name]
                pygame.draw.rect(surface, colour, pygame.Rect(x * tile_size[0], y * tile_size[1],
                                                              tile_size[0], tile_size[1]))

    # for player in board.players.values():
    #     x, y = player.get_tile_pos()
    #     w, h = board.get_tile_size()
    #     pygame.draw.rect(surface, (0, 104, 32), pygame.Rect(x * w, y * h, w, h))

    # for coord in board.flames:
    #     x, y = coord
    #     w, h = board.get_tile_size()
    #     pygame.draw.rect(surface, (207, 53, 46), pygame.Rect(x * w, y * h, w, h))

    #print(f"background: {background_blits}\tflame: {flame_blits}\tother: {other_tile_blits}")
    return surface


def draw_player(surface, player):
    global bomb_blits, player_blits
    w, h = player.get_size()
    tile_size = player.board.get_tile_size()

    for bomb in player.get_bombs():
        x, y = bomb.get_pos()
        birth = bomb.time_created
        rect = pygame.Rect((x, y), tile_size).move(-tile_size[0] // 2, -tile_size[1] // 2)
        frame = TILE_ANIMATIONS["bomb_entity"].get_current_frame(birth)
        surface.blit(frame, rect)
        bomb_blits += 1

    x, y = player.get_pos()
    state = player.state

    pid = player.player_id
    direction = player.movement_direction
    start = player.get_time_updated()

    rect = pygame.Rect((x, y), (w, h)).move(-tile_size[0] // 2, -tile_size[1])
    if state in ["stand", "walk", "punch"]:
        frame = PLAYER_ANIMATIONS[["white", "blue", "black", "red"][pid%4]][state][direction//2].get_current_frame(start)
    else:
        frame = PLAYER_ANIMATIONS[["white", "blue", "black", "red"][pid%4]][state].get_current_frame(start)

    surface.blit(frame, rect)
    player_blits += 1
    #pygame.draw.rect(surface, player.get_colour(), rect)


        #pygame.draw.rect(surface, (50, 50, 50), rect)
    #print(f"player: {player_blits}\tbomb: {bomb_blits}")
    return surface
